﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace CodeQuest
{
    public partial class Form1 : Form
    {
        Controller game;
        public Form1()
        {
            InitializeComponent();
            this.game = new Controller(this);
            this.game.menu();
            init_layout_board();
            init_layout_instruction_list(3);
            this.menu_panel.Location = new System.Drawing.Point(12, 12);
            this.win_panel.Location = new System.Drawing.Point(12, 12);
            this.menu_panel.Show();
            this.win_panel.Hide();
        }

        private void init_layout_instruction_list(int rows)
        {
            this.instructions_list.RowStyles.Clear();
            this.instructions_list.ColumnStyles.Clear();
            this.instructions_list.RowCount = rows;
            this.instructions_list.ColumnCount = 1;
            for (int i = 0; i < rows; i++)
            {
                instructions_list.RowStyles.Add(new RowStyle(SizeType.Absolute, 60));
            }
        }
        private void init_layout_board()
        {
            this.table_layou_board.RowStyles.Clear();
            this.table_layou_board.ColumnStyles.Clear();
            this.table_layou_board.ColumnCount = 20;
            this.table_layou_board.RowCount = 20;
            for (int i = 0; i < 20; i++)
            {
                table_layou_board.RowStyles.Add(new RowStyle(SizeType.AutoSize, 5F));
                table_layou_board.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize, 5F));
            }
            //TOREMOVE
            //for (int i = 0; i < 20; i++)
            //{
            //    Label label = new Label();
            //    label.Text = i + " - " + i;
            //    label.Size = new System.Drawing.Size(41, 41);
            //    label.BorderStyle = BorderStyle.FixedSingle;
            //    label.Margin = new System.Windows.Forms.Padding(0);
            //    table_layou_board.Controls.Add(label, i, i);
            //}

        }
        private Panel create_insctruction_box(string instruction_name)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Panel panel = new Panel();
            Label label = new Label();
            Button button_down = new Button();
            Button button_up = new Button();
            Button remove_button = new Button();

            remove_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            remove_button.Location = new System.Drawing.Point(7, 33);
            remove_button.Name = "remove_button";
            remove_button.Size = new System.Drawing.Size(19, 18);
            remove_button.TabIndex = 5;
            remove_button.Text = "X";
            remove_button.UseVisualStyleBackColor = true;
            remove_button.Click += new System.EventHandler(this.remove_button_Click);

            button_down.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            button_down.Location = new System.Drawing.Point(171, 31);
            button_down.Name = "arrow_down";
            button_down.Size = new System.Drawing.Size(35, 23);
            button_down.TabIndex = 1;
            button_down.Text = "↓";
            button_down.UseVisualStyleBackColor = true;
            button_down.Click += new System.EventHandler(this.arrow_down_Click);

            button_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            button_up.Location = new System.Drawing.Point(171, 3);
            button_up.Name = "arrow_up";
            button_up.Size = new System.Drawing.Size(35, 23);
            button_up.TabIndex = 4;
            button_up.Text = "↑";
            button_up.UseVisualStyleBackColor = true;
            button_up.Click += new System.EventHandler(this.arrow_up_Click);


            label.BackColor = System.Drawing.Color.Transparent;
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            label.ForeColor = System.Drawing.SystemColors.Control;
            label.Location = new System.Drawing.Point(29, 8);
            label.Name = "instruction_box_label";
            label.Size = new System.Drawing.Size(136, 43);
            label.TabIndex = 2;
            label.Text = instruction_name;
            label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            panel.BackColor = System.Drawing.SystemColors.HighlightText;
            panel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("instruction_box.BackgroundImage")));
            panel.Controls.Add(label);
            panel.Controls.Add(button_down);
            panel.Controls.Add(button_up);
            panel.Controls.Add(remove_button);
            panel.Name = "instruction_box";
            panel.Size = new System.Drawing.Size(209, 57);
            panel.TabIndex = 3;
            return panel;
        }

        private void remove_button_Click(object sender, EventArgs e)
        {
            if (sender is Control sender_c)
            {
                this.instructions_list.Controls.Remove(sender_c.Parent);
            }
        }

        private void add_button_Click(object sender, EventArgs e)
        {
            if (instruction_choose_list.Text.ToString() == String.Empty)
                return;

            Panel panel = create_insctruction_box(instruction_choose_list.Text.ToString());
            instructions_list.Controls.Add(panel);
        }

        private void arrow_up_Click(object sender, EventArgs e)
        {
            if (sender is Control sender_c)
            {
                this.instructions_list.Controls.SetChildIndex(sender_c.Parent, this.instructions_list.Controls.GetChildIndex(sender_c.Parent) - 1);
            }
        }

        private void arrow_down_Click(object sender, EventArgs e)
        {
            if (sender is Control sender_c)
            {
                this.instructions_list.Controls.SetChildIndex(sender_c.Parent, this.instructions_list.Controls.GetChildIndex(sender_c.Parent) + 1);
            }
        }

        private void run_1_level_Click(object sender, EventArgs e)
        {
            menu_panel.Hide();
            this.game.start();

        }

        private void start_button_Click(object sender, EventArgs e)
        {
            this.game.run();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.game.start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.game.menu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void quit_button_game_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
    abstract class Character
    {
        string name;
        int max_move;
        public int[] position;
        int steps_counter;
        public Label label;
        public Character()
        {
            this.position = new int[2];
        }
        void set_position(int x, int y) { }
        void move_to(int x, int y) { }
        abstract public void set_label();

    }
    class Player : Character
    {
        int points;
        string[] history;
        public Player() { }
        public override void set_label()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Label label = new Label();
            label.Image = ((System.Drawing.Image)(resources.GetObject("fox.Image")));
            label.BringToFront();
            label.TabIndex = 0;
            label.BackColor = System.Drawing.Color.Transparent;
            //label.Text = col + " - " + row;
            label.Size = new System.Drawing.Size(41, 41);
            label.Margin = new System.Windows.Forms.Padding(0);
            this.label = label;
        }
    }
    class Enemy : Character
    {
        string[] instructions;
        public Enemy() { }
        public override void set_label()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Label label = new Label();
            label.Image = ((System.Drawing.Image)(resources.GetObject("enemy.Image")));
            //label.Text = col + " - " + row;
            label.Size = new System.Drawing.Size(41, 41);
            label.Margin = new System.Windows.Forms.Padding(0);
            this.label = label;
        }
    }
    class Tile
    {
        string asset;
        bool include_point;
        bool include_player;
        int[] position;

        public Tile() { }
        void set_character(Character character) { }
        void set_position(int x, int y) { }
    }
    class FuncTile : Tile
    {
        string category;
        int type;
        string _event;

        public FuncTile() { }
        void execute(Board board) { }
    }

    class Board
    {
        Form1 form;
        int[] size;
        public int[] location;
        public int player_index;
        public int[][] board;
        public Character[] players;
        string[] tile_map = new string[]
        {
            "grass.Image",
            "water.Image",
            "hill.Image",
            "gem.Image",
            "tree.Image",
            "stone.Image"
        };

        public Board(Form1 form)
        {
            this.form = form;
            this.players = new Character[10];
            this.location = new int[] { 254, 12 };

        }
        void set_size(int x, int y) { }
        void set_tile(Tile tile, int x, int y) { }
        void update_board() { }
        public void set_character(Character chara, int index)
        {
            this.players[index] = chara;
            this.form.Controls.Add(chara.label);
        }
        public void set_player_index(int index)
        {
            this.player_index = index;
        }
        public void move_player(Character player, int x, int y)
        {
            this.form.table_layou_board.SuspendLayout();
            player.label.Location = new Point(x * 42 + this.location[0], y * 42 + this.location[1]);
            player.label.BringToFront();
            player.position[0] = x;
            player.position[1] = y;
            this.form.table_layou_board.ResumeLayout();
        }
        void set_up_asset(Label label, int asset_num)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));

            label.Image = ((System.Drawing.Image)(resources.GetObject(tile_map[asset_num])));
        }
        public void setup_board(int[][] board)
        {
            this.board = board;
            this.form.table_layou_board.SuspendLayout();
            Control[] s = new Control[20];
            for (int col = 0; col < 20; col++)
            {
                for (int row = 0; row < 20; row++)
                {
                    Label label = new Label();
                    label.Size = new System.Drawing.Size(41, 41);
                    label.Margin = new System.Windows.Forms.Padding(0);
                    set_up_asset(label, board[col][row]);
                    s[row] = label;
                }
                this.form.table_layou_board.Controls.AddRange(s);
            }
            this.form.table_layou_board.ResumeLayout();
        }
    }
    class Instruction
    {
        public string name;
        int category;
        int iterations;
        public Instruction() { }
        public void execute(Board board)
        {
            Character pl = board.players[board.player_index];


            if (name == "idź w górę")
            {
                if (pl.position[1] == 0) return;
                if (board.board[pl.position[1] - 1][pl.position[0]] != 0 && board.board[pl.position[1] - 1][pl.position[0]] != 3)
                    return;
                board.move_player(pl, pl.position[0], pl.position[1] - 1);
            }
            if (name == "idź w dół")
            {
                if (pl.position[1] == 19) return;
                if (board.board[pl.position[1] + 1][pl.position[0]] != 0 && board.board[pl.position[1] + 1][pl.position[0]] != 3)
                    return;
                board.move_player(pl, pl.position[0], pl.position[1] + 1);
            }
            if (name == "idź w prawo")
            {
                if (pl.position[0] == 19) return;
                if (board.board[pl.position[1]][pl.position[0] + 1] != 0 && board.board[pl.position[1]][pl.position[0] + 1] != 3)
                    return;
                board.move_player(pl, pl.position[0] + 1, pl.position[1]);
            }
            if (name == "idź w lewo")
            {
                if (pl.position[0] == 0) return;
                if (board.board[pl.position[1]][pl.position[0] - 1] != 0 && board.board[pl.position[1]][pl.position[0] - 1] != 3)
                    return;
                board.move_player(pl, pl.position[0] - 1, pl.position[1]);
            }
        }
    }
    class InstructionsCollection
    {
        int count;
        int max_instructions;
        Instruction[] instructions;
        int last_index = 0;
        public InstructionsCollection()
        {
            this.instructions = new Instruction[100];
        }
        public void set_instructions(Instruction instruction)
        {
            this.instructions[last_index] = instruction;
        }
        public void add_instruction(string name)
        {
            Instruction ins = new Instruction();
            ins.name = name;
            this.set_instructions(ins);
            last_index++;
        }
        public void clear()
        {
            this.instructions = new Instruction[100];
            this.last_index = 0;
        }
        public void exe(Board board)
        {
            for (int i = 0; i < this.last_index; i++)
            {
                this.instructions[i].execute(board);
            }

        }
    }
    //TODO
    class Controller
    {
        int time;
        Form1 form;
        public Board board;
        public InstructionsCollection instructions_collection;
        public Controller(Form1 form)
        {
            this.form = form;
        }
        void pause() { }
        public void start()
        {
            this.form.score_value.Text = "100";

            int[][] board = new[]
                 {
                    new[] {0, 0, 5, 2, 2, 5, 2, 2, 0, 4, 1, 0, 0, 4, 5, 1, 0, 0, 4, 5},
                    new[] {1, 4, 2, 0, 1, 0, 4, 2, 5, 0, 0, 1, 4, 5, 2, 0, 1, 0, 4, 5},
                    new[] {2, 5, 1, 4, 0, 0, 2, 1, 4, 5, 0, 2, 0, 1, 4, 5, 0, 2, 0, 1},
                    new[] {0, 0, 4, 5, 0, 2, 0, 0, 4, 5, 1, 2, 0, 0, 4, 5, 1, 2, 0, 0},
                    new[] {4, 1, 0, 0, 0, 4, 1, 0, 0, 5, 4, 1, 4, 0, 5, 4, 1, 0, 0, 5},
                    new[] {5, 2, 0, 1, 4, 5, 2, 0, 1, 4, 5, 2, 0, 1, 4, 3, 2, 0, 1, 4},
                    new[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    new[] {0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0},
                    new[] {0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0},
                    new[] {0, 0, 4, 5, 1, 0, 0, 0, 4, 5, 1, 2, 0, 0, 4, 5, 1, 2, 0, 0},
                    new[] {4, 1, 0, 0, 5, 4, 1, 0, 0, 5, 4, 1, 0, 0, 5, 4, 1, 0, 0, 5},
                    new[] {5, 2, 0, 1, 4, 5, 2, 0, 0, 4, 5, 2, 0, 0, 0, 0, 0, 0, 1, 4},
                    new[] {0, 0, 5, 2, 2, 5, 2, 2, 0, 4, 1, 0, 0, 4, 5, 1, 0, 0, 4, 5},
                    new[] {1, 4, 2, 0, 1, 0, 0, 0, 0, 0, 0, 1, 4, 5, 2, 0, 1, 0, 0, 5},
                    new[] {2, 5, 1, 4, 0, 0, 2, 1, 4, 5, 0, 2, 0, 1, 4, 5, 0, 2, 0, 1},
                    new[] {0, 0, 4, 5, 0, 2, 0, 0, 4, 5, 1, 2, 0, 0, 4, 5, 1, 2, 0, 0},
                    new[] {4, 1, 0, 0, 0, 4, 1, 0, 0, 5, 4, 1, 0, 0, 5, 4, 1, 0, 0, 5},
                    new[] {5, 2, 0, 1, 4, 5, 2, 0, 1, 4, 5, 2, 0, 1, 4, 5, 2, 0, 1, 4},
                    new[] {0, 0, 5, 2, 2, 5, 2, 2, 0, 4, 1, 0, 0, 4, 5, 1, 0, 0, 4, 5},
                    new[] {1, 4, 2, 0, 1, 0, 4, 2, 5, 0, 0, 1, 4, 5, 2, 0, 1, 0, 4, 5}
                };
            this.board = new Board(this.form);
            this.board.setup_board(board);
            Character player = new Player();
            player.set_label();
            this.board.set_character(player, 0);
            this.board.set_player_index(0);
            this.board.move_player(player, 10, 6);

            this.instructions_collection = new InstructionsCollection();
        }
        public void run()
        {
            this.form.score_value.Text = (Int32.Parse(this.form.score_value.Text.ToString()) - 10).ToString();
            if (Int32.Parse(this.form.score_value.Text.ToString()) - 10 == 0)
            {
                this.form.end_title.Text = "GAME OVER";
                this.form.win_panel.Show();
            }
            this.instructions_collection.clear();
            foreach (Control control in this.form.instructions_list.Controls)
            {
                foreach (Control control_s in control.Controls)
                {
                    if (control_s is Label label)
                    {
                        string content = label.Text;
                        this.instructions_collection.add_instruction(content);
                    }
                }
            }
            this.form.instructions_list.Controls.Clear();
            this.instructions_collection.exe(this.board);
            Character pl = board.players[board.player_index];
            Console.WriteLine(this.board.board[pl.position[1]][pl.position[0]]);
            if (this.board.board[pl.position[1]][pl.position[0]] == 3)
            {
                this.form.end_title.Text = "WIN";
                this.form.win_panel.Show();
            }
        }
        void stop() { }
        void show_leaderboard() { }
        void clear(){ }
        public void menu()
        {
            this.form.menu_panel.Show();
        }
    }
}
