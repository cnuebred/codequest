# CodeQuest

Edukacyjna gra wprowadzająca do świata algorytmów

### Cel projektu:
Celem projektu jest utworzenie gry „CodeQuest”, interaktywnej i angażującej platformy, która umożliwi młodym ludziom odkrywanie fascynującego świata programowania w sposób przyjazny i przystępny. Gra ma na celu zachęcić graczy do logicznego myślenia oraz rozwijania umiejętności programistycznych poprzez praktyczne doświadczenia i interakcję.

### Krótki opis planowanej gry:
Gracze wcielają się w rolę programistów, którzy mają za zadanie pomóc urokliwemu lisowi o imieniu Pixel pokonać różnorodne wyzwania na swojej drodze. W trakcie gry gracze będą mieli okazję wybierać odpowiednie kafelki z instrukcjami, a następnie ułożyć je w odpowiedniej kolejności. Ich celem jest zaprojektowanie algorytmów, które pozwolą sterowanej postaci pokonać daną trasę w jak najkrótszym czasie.
Gracze będą eksperymentować z różnymi instrukcjami programowania, takimi jak ruch do przodu, skręcanie w lewo lub w prawo, pętle i warunki logiczne. Każdy poziom gry będzie stanowił nowe wyzwanie, które wymaga od graczy logicznego myślenia, analizy sytuacji oraz kreatywnego podejścia do rozwiązywania problemów. Rozbudowany system podpowiedzi oraz możliwość eksperymentowania z różnymi kombinacjami instrukcji pomogą graczom zrozumieć podstawowe pojęcia programowania.

### Stan wiedzy

Edukacja w zakresie rozwiązywania problemów programistycznych odgrywa kluczową rolę w dzisiejszym świecie z uwagi na rosnące zapotrzebowanie na umiejętności związane z technologią oraz informatyką. Artykuły naukowe podkreślają, że nauka programowania rozwija umiejętność analitycznego myślenia, logicznego rozumowania i kreatywności, co przekłada się na szereg korzyści dla jednostki oraz społeczeństwa jako całości ([źródło](http://socialissues.cs.toronto.edu/index.html%3Fp=279.html)). Nauka rozwiązywania problemów programistycznych przyczynia się także do rozwijania umiejętności związanych z algorytmicznym myśleniem, co potwierdzają badania opublikowane w "[International Journal of Technology and Design Education](https://www.researchgate.net/journal/International-Journal-of-Technology-and-Design-Education-1573-1804?_tp=eyJjb250ZXh0Ijp7ImZpcnN0UGFnZSI6InB1YmxpY2F0aW9uIiwicGFnZSI6InB1YmxpY2F0aW9uIn19)"


### Przebieg gry
1. Gracz uruchamia grę i zostaje przedstawiony z wyborem poziomu.
2. Na każdym poziomie gracz ma za zadanie układać kafelki z instrukcjami programowania.
3. Gracz wybiera odpowiednie instrukcje, takie jak "Idź do przodu" czy "Skręć w prawo", i ustawia je w odpowiedniej kolejności.
4. Po ułożeniu instrukcji, gracz naciska przycisk "Start", aby uruchomić sterowaną postać.
5. Sterowana postać wykonuje ruchy zgodnie z ułożonymi instrukcjami, próbując pokonać trasę w jak najkrótszym czasie. Każdy następny poziom jest bardziej wymagający.
6. Gra podaje wynik gracza na podstawie czasu, który postać potrzebowała, aby ukończyć trasę.
7. Gracz otrzymuje punkty za szybkie i skuteczne ułożenie instrukcji oraz pokonanie trasy.

## FlowChart gry CodeQuest
```mermaid
gantt
    title CodeQuest FlowChart
    dateFormat  YYYY-MM-DD
    section Code
    Base                :a1, 2023-12-04, 5d
    Instructions        :after a1  , 7d
    Graphic             :after a1  , 4d
    section Graphic
    Menu        :2023-12-16  , 1d
    Bar         : 2d
    Board       : 2d
    Main Character      : 1d
```

## Koncept rozłożenia klas oraz funkcjionalności
```mermaid
classDiagram
    Character <|-- Player
    Character <|-- Enemy
    Character : +string name
    Character : +int max_move
    Character : +int[] position
    Character : +set_postition(int x, int y)
    Character : +move(string instruction)

    class Enemy{ 
        +int iteration
        +string[] instructions
    }
    class Player{
        +int points
        +int rest_move_steps
        +string[] history
    }
    class Tile{
      +string asset
      +bool include_point
      +bool include_player
      +int[] position
      +set_character(Character character)
      +set_postition(int x, int y)
    }
    class FunctionTile {
      +string category
      +int type
      +string event
      execute(Board board)
    }

    Tile <|-- FunctionTile

    class Board{
        +int[] size
        +int player_index
        +Tile[] tile_map
        +Character[] players
        +set_size(int x, int y)
        +set_tile(Tile tile, int x, int y)
        +update_board()
    }
    class Instruction{
        +string name
        +int category
        +int iterations
        +execute(Board board)
    }
    class InstructionBar{
        +int count
        +int max_instructions
        +Instruction[] instructions
        +set_instruction(Instruction instruction)
        +remove_instruction(int index)
        +change_queue(int from, int to)
    }

    class Game{
        init()
    } 
    class Controller{
        +int time
        pause()
        run()
        end()
        show_leaderboard()
    }
    Controller <|-- Game
```