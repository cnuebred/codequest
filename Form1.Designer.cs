﻿namespace CodeQuest
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.instruction_box = new System.Windows.Forms.Panel();
            this.remove_button = new System.Windows.Forms.Button();
            this.instruction_box_label = new System.Windows.Forms.Label();
            this.arrow_down = new System.Windows.Forms.Button();
            this.arrow_up = new System.Windows.Forms.Button();
            this.start_button = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.score_label = new System.Windows.Forms.Label();
            this.score_value = new System.Windows.Forms.Label();
            this.instruction_choose_list = new System.Windows.Forms.ComboBox();
            this.add_button = new System.Windows.Forms.Button();
            this.multiply_box_value = new System.Windows.Forms.NumericUpDown();
            this.instruction_label = new System.Windows.Forms.Label();
            this.multiply_label = new System.Windows.Forms.Label();
            this.table_layou_board = new System.Windows.Forms.TableLayoutPanel();
            this.instructions_list = new System.Windows.Forms.TableLayoutPanel();
            this.menu_panel = new System.Windows.Forms.Panel();
            this.quit_button_game = new System.Windows.Forms.Button();
            this.run_1_level = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.win_panel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.end_title = new System.Windows.Forms.Label();
            this.grass = new System.Windows.Forms.Label();
            this.water = new System.Windows.Forms.Label();
            this.hill = new System.Windows.Forms.Label();
            this.gem = new System.Windows.Forms.Label();
            this.tree = new System.Windows.Forms.Label();
            this.stone = new System.Windows.Forms.Label();
            this.fox = new System.Windows.Forms.Label();
            this.instruction_box.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.multiply_box_value)).BeginInit();
            this.menu_panel.SuspendLayout();
            this.win_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // instruction_box
            // 
            this.instruction_box.BackColor = System.Drawing.SystemColors.HighlightText;
            this.instruction_box.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("instruction_box.BackgroundImage")));
            this.instruction_box.Controls.Add(this.remove_button);
            this.instruction_box.Controls.Add(this.instruction_box_label);
            this.instruction_box.Controls.Add(this.arrow_down);
            this.instruction_box.Controls.Add(this.arrow_up);
            this.instruction_box.Location = new System.Drawing.Point(1492, 12);
            this.instruction_box.Name = "instruction_box";
            this.instruction_box.Size = new System.Drawing.Size(209, 57);
            this.instruction_box.TabIndex = 3;
            // 
            // remove_button
            // 
            this.remove_button.BackColor = System.Drawing.Color.White;
            this.remove_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.remove_button.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.remove_button.FlatAppearance.BorderSize = 15;
            this.remove_button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.remove_button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.remove_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.remove_button.Location = new System.Drawing.Point(7, 33);
            this.remove_button.Margin = new System.Windows.Forms.Padding(0);
            this.remove_button.Name = "remove_button";
            this.remove_button.Size = new System.Drawing.Size(19, 18);
            this.remove_button.TabIndex = 3;
            this.remove_button.Text = "X";
            this.remove_button.UseVisualStyleBackColor = false;
            // 
            // instruction_box_label
            // 
            this.instruction_box_label.BackColor = System.Drawing.Color.Transparent;
            this.instruction_box_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.instruction_box_label.ForeColor = System.Drawing.SystemColors.Control;
            this.instruction_box_label.Location = new System.Drawing.Point(29, 8);
            this.instruction_box_label.Name = "instruction_box_label";
            this.instruction_box_label.Size = new System.Drawing.Size(136, 43);
            this.instruction_box_label.TabIndex = 2;
            this.instruction_box_label.Text = "Instruction";
            this.instruction_box_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // arrow_down
            // 
            this.arrow_down.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.arrow_down.Location = new System.Drawing.Point(171, 31);
            this.arrow_down.Name = "arrow_down";
            this.arrow_down.Size = new System.Drawing.Size(35, 23);
            this.arrow_down.TabIndex = 1;
            this.arrow_down.Text = "↓";
            this.arrow_down.UseVisualStyleBackColor = true;
            this.arrow_down.Click += new System.EventHandler(this.arrow_down_Click);
            // 
            // arrow_up
            // 
            this.arrow_up.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.arrow_up.BackColor = System.Drawing.Color.Transparent;
            this.arrow_up.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.arrow_up.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.arrow_up.FlatAppearance.BorderSize = 15;
            this.arrow_up.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.arrow_up.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.arrow_up.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.arrow_up.Location = new System.Drawing.Point(171, 3);
            this.arrow_up.Margin = new System.Windows.Forms.Padding(0);
            this.arrow_up.Name = "arrow_up";
            this.arrow_up.Size = new System.Drawing.Size(35, 23);
            this.arrow_up.TabIndex = 0;
            this.arrow_up.Text = "↑";
            this.arrow_up.UseVisualStyleBackColor = false;
            this.arrow_up.Click += new System.EventHandler(this.arrow_up_Click);
            // 
            // start_button
            // 
            this.start_button.Location = new System.Drawing.Point(171, 12);
            this.start_button.Name = "start_button";
            this.start_button.Size = new System.Drawing.Size(64, 31);
            this.start_button.TabIndex = 4;
            this.start_button.Text = "Start";
            this.start_button.UseVisualStyleBackColor = true;
            this.start_button.Click += new System.EventHandler(this.start_button_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(19, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(61, 31);
            this.button5.TabIndex = 5;
            this.button5.Text = "Menu";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // score_label
            // 
            this.score_label.AutoSize = true;
            this.score_label.BackColor = System.Drawing.Color.Transparent;
            this.score_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.score_label.Location = new System.Drawing.Point(97, 13);
            this.score_label.Name = "score_label";
            this.score_label.Size = new System.Drawing.Size(61, 20);
            this.score_label.TabIndex = 6;
            this.score_label.Text = "Score:";
            // 
            // score_value
            // 
            this.score_value.AutoSize = true;
            this.score_value.BackColor = System.Drawing.Color.Transparent;
            this.score_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.score_value.Location = new System.Drawing.Point(119, 29);
            this.score_value.Name = "score_value";
            this.score_value.Size = new System.Drawing.Size(19, 20);
            this.score_value.TabIndex = 7;
            this.score_value.Text = "0";
            // 
            // instruction_choose_list
            // 
            this.instruction_choose_list.FormattingEnabled = true;
            this.instruction_choose_list.Items.AddRange(new object[] {
            "idź w górę",
            "idź w dół",
            "idź w prawo",
            "idź w lewo"});
            this.instruction_choose_list.Location = new System.Drawing.Point(22, 73);
            this.instruction_choose_list.Name = "instruction_choose_list";
            this.instruction_choose_list.Size = new System.Drawing.Size(143, 21);
            this.instruction_choose_list.TabIndex = 9;
            // 
            // add_button
            // 
            this.add_button.Location = new System.Drawing.Point(171, 67);
            this.add_button.Name = "add_button";
            this.add_button.Size = new System.Drawing.Size(64, 31);
            this.add_button.TabIndex = 10;
            this.add_button.Text = "Add";
            this.add_button.UseVisualStyleBackColor = true;
            this.add_button.Click += new System.EventHandler(this.add_button_Click);
            // 
            // multiply_box_value
            // 
            this.multiply_box_value.Location = new System.Drawing.Point(22, 113);
            this.multiply_box_value.Name = "multiply_box_value";
            this.multiply_box_value.Size = new System.Drawing.Size(64, 20);
            this.multiply_box_value.TabIndex = 11;
            // 
            // instruction_label
            // 
            this.instruction_label.AutoSize = true;
            this.instruction_label.BackColor = System.Drawing.Color.Transparent;
            this.instruction_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.instruction_label.Location = new System.Drawing.Point(26, 57);
            this.instruction_label.Name = "instruction_label";
            this.instruction_label.Size = new System.Drawing.Size(125, 13);
            this.instruction_label.TabIndex = 12;
            this.instruction_label.Text = "Dostępne Instrukcje:";
            // 
            // multiply_label
            // 
            this.multiply_label.AutoSize = true;
            this.multiply_label.BackColor = System.Drawing.Color.Transparent;
            this.multiply_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.multiply_label.Location = new System.Drawing.Point(26, 97);
            this.multiply_label.Name = "multiply_label";
            this.multiply_label.Size = new System.Drawing.Size(106, 13);
            this.multiply_label.TabIndex = 13;
            this.multiply_label.Text = "Liczba powtórzeń";
            // 
            // table_layou_board
            // 
            this.table_layou_board.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.table_layou_board.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.table_layou_board.ColumnCount = 1;
            this.table_layou_board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.table_layou_board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_layou_board.Location = new System.Drawing.Point(254, 12);
            this.table_layou_board.Margin = new System.Windows.Forms.Padding(0);
            this.table_layou_board.Name = "table_layou_board";
            this.table_layou_board.RowCount = 1;
            this.table_layou_board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.table_layou_board.Size = new System.Drawing.Size(840, 840);
            this.table_layou_board.TabIndex = 14;
            // 
            // instructions_list
            // 
            this.instructions_list.AutoScroll = true;
            this.instructions_list.ColumnCount = 1;
            this.instructions_list.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.instructions_list.Location = new System.Drawing.Point(19, 154);
            this.instructions_list.Name = "instructions_list";
            this.instructions_list.RowCount = 1;
            this.instructions_list.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.instructions_list.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 698F));
            this.instructions_list.Size = new System.Drawing.Size(216, 698);
            this.instructions_list.TabIndex = 4;
            // 
            // menu_panel
            // 
            this.menu_panel.BackColor = System.Drawing.Color.Transparent;
            this.menu_panel.Controls.Add(this.quit_button_game);
            this.menu_panel.Controls.Add(this.run_1_level);
            this.menu_panel.Controls.Add(this.label1);
            this.menu_panel.Location = new System.Drawing.Point(1120, 75);
            this.menu_panel.Name = "menu_panel";
            this.menu_panel.Size = new System.Drawing.Size(1082, 840);
            this.menu_panel.TabIndex = 15;
            // 
            // quit_button_game
            // 
            this.quit_button_game.FlatAppearance.BorderSize = 0;
            this.quit_button_game.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.quit_button_game.Location = new System.Drawing.Point(445, 250);
            this.quit_button_game.Name = "quit_button_game";
            this.quit_button_game.Size = new System.Drawing.Size(187, 48);
            this.quit_button_game.TabIndex = 2;
            this.quit_button_game.Text = "QUIT";
            this.quit_button_game.UseVisualStyleBackColor = true;
            this.quit_button_game.Click += new System.EventHandler(this.quit_button_game_Click);
            // 
            // run_1_level
            // 
            this.run_1_level.FlatAppearance.BorderSize = 0;
            this.run_1_level.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.run_1_level.Location = new System.Drawing.Point(445, 189);
            this.run_1_level.Name = "run_1_level";
            this.run_1_level.Size = new System.Drawing.Size(187, 45);
            this.run_1_level.TabIndex = 1;
            this.run_1_level.Text = "START GAME";
            this.run_1_level.UseVisualStyleBackColor = true;
            this.run_1_level.Click += new System.EventHandler(this.run_1_level_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 52F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(392, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(305, 84);
            this.label1.TabIndex = 0;
            this.label1.Text = "CodeQuest";
            // 
            // win_panel
            // 
            this.win_panel.BackColor = System.Drawing.Color.Transparent;
            this.win_panel.Controls.Add(this.button1);
            this.win_panel.Controls.Add(this.button2);
            this.win_panel.Controls.Add(this.end_title);
            this.win_panel.Location = new System.Drawing.Point(1108, 529);
            this.win_panel.Name = "win_panel";
            this.win_panel.Size = new System.Drawing.Size(1082, 840);
            this.win_panel.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(445, 250);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(187, 48);
            this.button1.TabIndex = 2;
            this.button1.Text = "QUIT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(445, 189);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 45);
            this.button2.TabIndex = 1;
            this.button2.Text = "RESTART";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // end_title
            // 
            this.end_title.AutoSize = true;
            this.end_title.Font = new System.Drawing.Font("Monotype Corsiva", 52F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.end_title.Location = new System.Drawing.Point(442, 36);
            this.end_title.Name = "end_title";
            this.end_title.Size = new System.Drawing.Size(176, 84);
            this.end_title.TabIndex = 0;
            this.end_title.Text = "WIN";
            // 
            // grass
            // 
            this.grass.BackColor = System.Drawing.Color.Transparent;
            this.grass.Image = ((System.Drawing.Image)(resources.GetObject("grass.Image")));
            this.grass.Location = new System.Drawing.Point(1442, 201);
            this.grass.Name = "grass";
            this.grass.Size = new System.Drawing.Size(42, 42);
            this.grass.TabIndex = 16;
            // 
            // water
            // 
            this.water.BackColor = System.Drawing.Color.Transparent;
            this.water.Image = ((System.Drawing.Image)(resources.GetObject("water.Image")));
            this.water.Location = new System.Drawing.Point(1442, 262);
            this.water.Name = "water";
            this.water.Size = new System.Drawing.Size(42, 42);
            this.water.TabIndex = 17;
            // 
            // hill
            // 
            this.hill.BackColor = System.Drawing.Color.Transparent;
            this.hill.Image = ((System.Drawing.Image)(resources.GetObject("hill.Image")));
            this.hill.Location = new System.Drawing.Point(1442, 323);
            this.hill.Margin = new System.Windows.Forms.Padding(0);
            this.hill.Name = "hill";
            this.hill.Size = new System.Drawing.Size(42, 42);
            this.hill.TabIndex = 18;
            // 
            // gem
            // 
            this.gem.BackColor = System.Drawing.Color.Transparent;
            this.gem.Image = ((System.Drawing.Image)(resources.GetObject("gem.Image")));
            this.gem.Location = new System.Drawing.Point(1442, 386);
            this.gem.Margin = new System.Windows.Forms.Padding(0);
            this.gem.Name = "gem";
            this.gem.Size = new System.Drawing.Size(42, 42);
            this.gem.TabIndex = 19;
            // 
            // tree
            // 
            this.tree.BackColor = System.Drawing.Color.Transparent;
            this.tree.Image = ((System.Drawing.Image)(resources.GetObject("tree.Image")));
            this.tree.Location = new System.Drawing.Point(1442, 463);
            this.tree.Margin = new System.Windows.Forms.Padding(0);
            this.tree.Name = "tree";
            this.tree.Size = new System.Drawing.Size(42, 42);
            this.tree.TabIndex = 20;
            // 
            // stone
            // 
            this.stone.BackColor = System.Drawing.Color.Transparent;
            this.stone.Image = ((System.Drawing.Image)(resources.GetObject("stone.Image")));
            this.stone.Location = new System.Drawing.Point(1442, 529);
            this.stone.Margin = new System.Windows.Forms.Padding(0);
            this.stone.Name = "stone";
            this.stone.Size = new System.Drawing.Size(42, 42);
            this.stone.TabIndex = 21;
            // 
            // fox
            // 
            this.fox.BackColor = System.Drawing.Color.Transparent;
            this.fox.Image = ((System.Drawing.Image)(resources.GetObject("fox.Image")));
            this.fox.Location = new System.Drawing.Point(1442, 606);
            this.fox.Margin = new System.Windows.Forms.Padding(0);
            this.fox.Name = "fox";
            this.fox.Size = new System.Drawing.Size(42, 42);
            this.fox.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1098, 856);
            this.Controls.Add(this.win_panel);
            this.Controls.Add(this.fox);
            this.Controls.Add(this.stone);
            this.Controls.Add(this.tree);
            this.Controls.Add(this.gem);
            this.Controls.Add(this.hill);
            this.Controls.Add(this.water);
            this.Controls.Add(this.grass);
            this.Controls.Add(this.menu_panel);
            this.Controls.Add(this.instruction_box);
            this.Controls.Add(this.instructions_list);
            this.Controls.Add(this.table_layou_board);
            this.Controls.Add(this.multiply_label);
            this.Controls.Add(this.instruction_label);
            this.Controls.Add(this.multiply_box_value);
            this.Controls.Add(this.add_button);
            this.Controls.Add(this.instruction_choose_list);
            this.Controls.Add(this.score_value);
            this.Controls.Add(this.score_label);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.start_button);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.instruction_box.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.multiply_box_value)).EndInit();
            this.menu_panel.ResumeLayout(false);
            this.menu_panel.PerformLayout();
            this.win_panel.ResumeLayout(false);
            this.win_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Panel instruction_box;
        public System.Windows.Forms.Label instruction_box_label;
        public System.Windows.Forms.Button arrow_down;
        public System.Windows.Forms.Button arrow_up;
        public System.Windows.Forms.Button start_button;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Label score_label;
        public System.Windows.Forms.Label score_value;
        public System.Windows.Forms.ComboBox instruction_choose_list;
        public System.Windows.Forms.Button add_button;
        public System.Windows.Forms.NumericUpDown multiply_box_value;
        public System.Windows.Forms.Label instruction_label;
        public System.Windows.Forms.Label multiply_label;
        public System.Windows.Forms.TableLayoutPanel table_layou_board;
        public System.Windows.Forms.TableLayoutPanel instructions_list;
        public System.Windows.Forms.Button remove_button;
        public System.Windows.Forms.Panel menu_panel;
        public System.Windows.Forms.Button run_1_level;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button quit_button_game;
        private System.Windows.Forms.Label grass;
        private System.Windows.Forms.Label water;
        private System.Windows.Forms.Label hill;
        private System.Windows.Forms.Label gem;
        private System.Windows.Forms.Label tree;
        private System.Windows.Forms.Label stone;
        private System.Windows.Forms.Label fox;
        public System.Windows.Forms.Panel win_panel;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Label end_title;
    }
}

